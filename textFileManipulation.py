#import re
#ptn=re.compile("\s+") #looks for a space or tab char at least once
#fh=open("hosts.real","r")
#A.Remove all of the comments from the file
#ptn2=re.compile("#.*")
#for line in fh:
    #remove lines that begin with an a hash #
#    if re.search('^#',line):
#        continue
#    if re.search('#',line):
#        line=ptn2.sub("",line)

#    print(line.rstrip("\r\n"))
#fh.close()
#print ("Finished A, starting B.")

#B.Extract all of the MAC addresses from the file

#macNum=re.compile("([0-9a-fA-F]:?){12}")
#textFH=open("hosts.real","r")

#textFH.readline()
#for MAC in textFH:
#    MAC=macNum.search(MAC)
#    if MAC:
#        print(MAC.group().replace(",",""))
#textFH.close()


#ptn=re.compile("..:..:..:..:..")
#ptn=re.compile("[0-9]{2,}:[0-9]{2,}:[a-zA-Z0-9]{2,}:[a-zA-Z0-9]{2,}:[a-zA-Z0-9]{2,}:[0-9]{2,}", re.IGNORECASE) #ignores case


#print ("Finished B, starting C.")
#C.Finally remove all the extraneous spaces and write the IP addresses to a new file.
#textFH=open("hosts.real","r")
#outTextFH=open("textOutput.txt","w") #saving the contents to another file
#for * in textFH:

import re #allows the use of regex
import shutil #used to move the files

removespace=re.compile("\s+") #looks for a space or tab char at least once
removecomment=re.compile("#.*")#looks for #
macNum=re.compile("([0-9a-fA-F]:?){12}") #regex for MAC
ipAdd=re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}") #Regex for IP address

fh=open("hosts.real","r")
outputfh=open("ipAddress.txt","w") #ip addresses
newfh=open("newfile.txt","w") #removed comments in an output file

fh.readline()
for line in fh:
    #remove lines that begin with an a#
    #if re.search('^#',line):
    #    line=removecomment.sub("",line)
    if re.search('^#',line):
        continue
    if re.search('#',line):
        line=removecomment.sub("",line)

        #getting the mac addresses
    mac=macNum.search(line)
    if mac:
        print(mac.group().replace(",",""))

        #getting the IP addresses and writing them to the other file
    ip=ipAdd.search(line)
    if ip:
        outputfh.write(ip.group()+"\n")

        #Remove a space
    if re.search("\s+",line):
        line=removespace.sub(" ",line)
    newfh.write(line+"\n")

fh.close()
outputfh.close()
newfh.close()

shutil.move('newfile.txt','hosts.real')
